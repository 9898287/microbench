﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.IO;

class PrimeSieve
{
    private int SieveSize;
    private BitArray BitArray;
    private Dictionary<int, int> MyDic = new()
    {
        { 10, 4 },
        { 100, 25 },
        { 1000, 168 },
        { 10000, 1229 },
        { 100000, 9592 },
        { 1000000, 78498 },
        { 10000000, 664579 },
        { 100000000, 5761455 },
        { 1000000000, 50847534 }
    };

    public PrimeSieve(int sieveSize)
    {
        SieveSize = sieveSize;
        BitArray = new BitArray(sieveSize, true);
    }

    public void RunSieve()
    {
        var factor = 3;
        var q = (int)Math.Sqrt(SieveSize);
        while (factor <= q)
        {
            for (int num = factor; num < SieveSize; num += 2)
            {
                if (BitArray[num])
                {
                    factor = num;
                    break;
                }
            }
            for (int num = factor * factor; num < SieveSize; num += factor * 2)
                BitArray[num] = false;
            factor += 2;
        }
    }
    public void PrintResults(bool showResults, double duration, int passes)
    {
        if (showResults)
            Console.Write("2, ");
        int count = 1;
        for (int num = 3; num < SieveSize; num += 2)
        {
            if (BitArray[num])
            {
                if (showResults)
                    Console.Write(num + ", ");
                count++;
            }
        }
        if (showResults)
            Console.WriteLine("");
        Console.WriteLine("Passes: " + passes + ", Time: " + duration + ", Avg: " + (duration / passes) + ", Limit: " + SieveSize + ", Count: " + count + ", Valid: " + ValidateResults());
    }
    public bool ValidateResults()
    {
        if (MyDic.ContainsKey(SieveSize))
            return MyDic[SieveSize] == CountPrimes();
        return false;
    }

    public int CountPrimes()
    {
        int count = 1;
        for (int i = 3; i < SieveSize; i += 2)
        {
            if (BitArray[i])
                count++;
        }

        return count;
    }
    private static void Main(string[] args)
    {
        var passes = 0;
        var tStart = DateTime.UtcNow;
        while (true)
        {
            var sieve = new PrimeSieve(1000000);
            sieve.RunSieve();
            passes++;
            var elapsed = (DateTime.UtcNow - tStart).TotalSeconds;
            if (elapsed >= int.Parse(args[0]))
            {
                sieve.PrintResults(false, elapsed, passes);
                using StreamWriter strm = File.AppendText(args[1]);
                strm.WriteLine($"C#({passes}),{passes}");
                break;
            }
        }
    }
}
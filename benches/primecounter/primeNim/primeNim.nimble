# Package

version       = "0.1.0"
author        = "adnan338"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["primeNim"]
binDir        = "bin"


# Dependencies

requires "nim >= 1.4.4"

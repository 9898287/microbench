## Count the most Prime numbers under given number of seconds

- Validation required

- First CLI argument will be time (3 - 7s) and the second argument is the `csv` file 
where the program will write the number of counted primes. The format is
`${impl_title}(${prime_count}),${prime_count}\n`